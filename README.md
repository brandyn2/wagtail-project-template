## Infrastructure

### Requirements

1. Terraform installed
1. Digital Ocean Token (from Digital Ocean account dashboard)
1. Local ssh key pair

### Getting Started

Terraform will provision the infrastructure, which is currently a single Digital Ocean droplet with ssh access (root password login is disabled for security).

1. Set Terraform Variables
   ```sh
   # enter the infrastructure directory.
   # all following instructions assume you are
   # in the infrastructure directory.
   cd infrastructure
   # copy the example file to a gitignored file, then set the appropriate variables
   cp example.terraform.tfvars terraform.tfvars
   ```
1. Initialize Terraform

   ```sh
   terraform init
   ```

1. Provision infrastructure

   ```sh
   # check what resources will be created
   make plan

   # provision those resources if they look good to you
   make infrastructure
   ```

1. SSH into the newly created server

   ```sh
   # this will read the new IP address from terraform
   # and you'll be inside the remote server
   make ssh
   ```

1. Delete the resources to cleanup and save $
   ```sh
   make clean
   ```
