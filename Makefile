NS=brandyn
PROJECT=wagtail
VERSION:=0.1
IMAGE=${NS}/${PROJECT}:${VERSION}

APP=wagtail_docker_demo

dev: build
	docker run --rm -it ${IMAGE}

shell:
	docker run --rm -it \
	-v $(PWD)/scripts:/usr/app/scripts \
	-v $(PWD)/app:/usr/app \
	 ${IMAGE} bash

initialize-site:
	docker run --rm -it \
		-v $(PWD)/app:/usr/app \
		${IMAGE} wagtail start ${APP} app

build:
	docker build -t ${IMAGE} -f docker/Dockerfile .