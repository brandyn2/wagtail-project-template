############################################
### Initial Terraform + Digital Ocean Setup
############################################
terraform {
  required_version = ">= 0.14"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = ">= 2.6.0"
    }
  }
}

# configure digital ocean provider
provider "digitalocean" {
  token = var.do_token
}


############################################
### Define Digital Ocean Cloud Resources
############################################

# Create a new SSH key in your digital ocean account
# Allows us to add this ssh key to the new droplet below for ssh access
resource "digitalocean_ssh_key" "default" {
  name       = "terraform"
  public_key = file(var.do_public_ssh_key)
}

# example web server droplet
resource "digitalocean_droplet" "www-1" {
  image              = "ubuntu-20-04-x64"
  name               = "www-1"
  region             = var.do_region
  size               = "s-1vcpu-1gb"
  private_networking = true
  # root password login will be disabled by default (check /etc/ssh/sshd_config to confirm)
  ssh_keys = [
    digitalocean_ssh_key.default.id
  ]
}

# Consider creating a "droplet" module, and having default outputs in a separate file.
output "droplet_ip_addr" {
  value = digitalocean_droplet.www-1.ipv4_address
}

output "droplet_id" {
  value       = digitalocean_droplet.www-1.id
  description = "ID of the www-1 droplet"
}

