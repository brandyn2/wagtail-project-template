# Copy this file and rename to "terraform.tfvars".
# Terraform will automatically load this variables for you

# personal access token that authenticates you with Digital Ocean to provision and destroy resources
do_token = ""
# path to your public ssh key to upload to terraform (ex. /user/home/.ssh/id.pub)
do_public_ssh_key = ""