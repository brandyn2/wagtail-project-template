variable "do_token" {
  type        = string
  description = "Digital Ocean token for authentication."
}

variable "do_public_ssh_key" {
  type        = string
  description = "Path to your local public ssh key, to be uploaded to Digital Ocean and loaded onto servers for authenticating"
}

variable "do_region" {
  type        = string
  default     = "nyc1"
  description = "Region to provision Digital Ocean resources."
}