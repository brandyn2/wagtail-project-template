# https://docs.wagtail.io/en/stable/getting_started/tutorial.html
FROM python:3
LABEL maintainer="Brandyn Friedly"

# # Install system packages
# RUN apk --no-cache add \
# 	# Pillow Dependenies (https://github.com/python-pillow/docker-images/blob/b4f47e252c716dd6649f3b2baac16119fa26c6fc/alpine/Dockerfile#L21) \
# 	gcc \
# 	freetype-dev \
# 	fribidi-dev \
# 	harfbuzz-dev \
# 	jpeg-dev \
# 	lcms2-dev \
# 	openjpeg-dev \
# 	tcl-dev \
# 	tiff-dev \
# 	tk-dev \
# 	zlib-dev

WORKDIR /usr/app
# Install python requirements
COPY requirements.txt .
RUN pip install -r requirements.txt

CMD ["python"]